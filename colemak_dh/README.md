``` bash
sudo cp us /usr/share/X11/xkb/symbols/us
```

Then `setxkbmap us colemak_dh -model ppkb` can be used in X11.
