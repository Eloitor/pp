set -x

sudo apk update
sudo apk upgrade

sudo apk add lyx osmin mnemosyne flatpak audiotube
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub io.github.kotatogram
flatpak install flathub io.freetubeapp.FreeTube
flatpak override --user --env=GDK_BACKEND=x11 io.freetubeapp.FreeTube

cd /usr/share/applications/
echo "modify chromium.desktop"
echo "Exec= chromium-browser %U --enable-features=UseOzonePlatform --ozone-platform=wayland --start-maximized  --user-agent="Mozilla/5.0 (Linux; Android 4.4.2; Nexus 5 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.99 Mobile Safari/537.36"
